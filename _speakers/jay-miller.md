---
name: Jay Miller
talks:
- "Making Government Useable by anyone with Pandas, Python and Eland"
---
Jay is a Developer Advocate at Elastic, based in San Diego, Ca. A
multipotentialite, Jay enjoys finding unique ways to merge his fascination
with productivity, automation, and development to create tools and content
to serve the tech community.
