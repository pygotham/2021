---
name: R.Kracekumar
talks:
- "Python Typing Koans"
---
I'm a FOSS enthusiast and programmer with an interest in back-end
technologies. I have worked on web-applications, data analysis, and machine
learning.
